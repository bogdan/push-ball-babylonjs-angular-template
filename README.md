BAYBLONJS TEMPLATE - WITH AMMO PHYSICS
--------------------------------------

-LOKACIJE NA DISKU (Angular Projekt): 
C:\Users\Bogdan\WebstormProjects\BabylonJS\roll-ball-babylonjs-angular
-BLENDER FIBONACCI 
C:\Users\Bogdan\Documents\BlenderModels\Fibonacci\
C:\Users\Bogdan\Documents\BlenderModels\Push_Ball_Level1\


HOW TO run
$ npm run start
$ npm run start3G
$ npm run start4G
$ npm build

$ cd C:\Users\Bogdan\WebstormProjects\BabylonJS\angular-babylonjs-templat\
$ npm run start -- --host 192.168.1.73 --port 4210
$ npm run start3G -- --host 192.168.1.73 --port 4210
$ npm run start4G -- --host 192.168.1.73 --port 4210

Then in browser go to:
http://192.168.1.73:4210/
http://localhost:4200/


BABYLONJS
---------
BabylonJS: Documents URLs
  -> https://doc.babylonjs.com/divingDeeper/physics
  -> https://doc.babylonjs.com/divingDeeper/physics/usingPhysicsEngine
  

Bullet Physics SDK Official Sites
  --> https://pybullet.org/wordpress/
  --> git: https://github.com/bulletphysics/bullet3

-- AMMO --
  --> https://github.com/kripken/ammo.js/  
  --> https://github.com/kripken/ammo.js/blob/main/examples/webgl_demo/ammo.html#L14

-- JavaScript PROMISE ERROR HANDLE-ING
https://github.com/RaananW/babylonjs-webpack-es6/blob/master/package.json
https://javascript.info/promise-error-handling

WINDOWS SITE FOR BABYLONJS DOCUMENTATION ONLINE:
-> https://babylonjsguide.github.io/
-> https://docs.microsoft.com/en-us/archive/msdn-magazine/2015/december/game-development-babylon-js-building-a-basic-game-for-the-web



BLENDER
-------
-Note: Pazi ko delaš objekte v Blender-ju. Da postaviš prav ORIGIN, ker je fizika odvisna od ORIGIN-a !!!

-glTF (format fajla ki vsebuje 3D stvari)
  --> https://github.com/KhronosGroup/glTF-Blender-IO
  --> https://docs.blender.org/manual/en/3.0/addons/import_export/scene_gltf2.html
  --> https://github.com/KhronosGroup/glTF-Blender-Exporter/blob/master/README.md



$ npm install --save-dev @babylonjs/core @babylonjs/loaders

GLTF/GLB
--------
vedno Export-aj in Load-aj .glb 
.glb se je rešilo. 
To se rešuje v Blender-ju.
1.) Narediš Material za PBR Material Node Editor-jem.
2.) Bake-aš tri Layer-e (1. Defuse/Color 2. Roughtness, 3.Normal)
3.) GLEJ VIDEO: https://www.youtube.com/watch?v=eYvgFWEiNp8 (TUTORIAL HOW BLENDER BAKE TEXTURE ) DOBER VIDEO !!!!


Vedno Exportaj glb fajl, ker vsebuje texture/material-e ...




POGLEJ ŠE ČE SE DA CUSTOM WEBPACK NAŠTIMAT IN FIZIKA CANNON POTEM !!!
https://www.npmjs.com/package/@angular-builders/custom-webpack

AMMO:
https://codesandbox.io/examples/package/ammo-node
tal je težka oa webassembly in ka vem kaj
https://forum.babylonjs.com/t/ammo-typeerror-on-reloading-playground-need-to-click-play-button-to-bypass-error/16891/13

Izbrisal iz package.json in zamenjal z drugo
"dependencies": {
..
  "@babylonjs/core": "^5.0.0-beta.6",
  "@babylonjs/materials": "^5.0.0-beta.6",
..
}


TODO:
-----
HOW TO LOAD .gltf file with BabylonJS
Template je osnova (še ne prečiščena koda)
Angular Project + BabylonJS (v5.0.0-beta.9 - WebGL - Parallel shader compilation)

1.) Menu or tumbe icons... Which Enable Switch-ing Between Diffrenet Cameras ArcCamera, 
    Universal Camera, 
    Arc Rotate Camera , 
    Follow Camera, 
    Anaglyph Camera, 
    Device Orientation Cameras
    Virtual Joysticks Camera
    Virtual Reality Camera
    ( I don't know if there is any another specific Augmented Camera ...)

2.) Enable Multi Controler Keyboard,Mouse,GamePad, Hands (for VR) (Switching in template project)

3.) Switch-ing between Diffrent SkyBox-es SkySpheres

4.) Switch-ing Between Different Implementation of Physics (Oimo, Cannon, Ammo, ....)

5.) Upgrade Angular Version with BabylonJS version...

6.) Maybe Enable Switching (throught Menu) Loading (uplaod-in) gltf, obj, babylon, and other exported types of file, to to import scenes...

Cleaning Up code. Arhitcture ..., This "Template Project" as One Alternative, to code games and other thins Virtual Galery Arhitctures....

# ng-babylon

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) and is designed as a basic template for [BabylonJS](https://www.babylonjs.com/) combined with [Angular](https://angular.io/) and [Bootstrap](https://getbootstrap.com/) in Version 4.0

The project is setup to use global [SCSS](https://sass-lang.com/) only and [ViewEncapsulation.None](https://angular.io/api/core/ViewEncapsulation).

Feel free to do anything you want with this template.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Choose your favorite e2e testing framework  
Recommendation: [Cypress Angular Schematic](https://www.npmjs.com/package/@cypress/schematic).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
