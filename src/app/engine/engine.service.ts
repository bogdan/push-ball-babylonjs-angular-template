import { WindowRefService } from '../services/window-ref.service';
import {ElementRef, Injectable, NgZone} from '@angular/core';
/*
// DELNO IMPORT-ANJE
import {
  // BABYLON.MeshBuilder.CreateSphere
  // BABYLON, // NE MOREM IMPORTAT - ZAKAJ

  Engine,
  FreeCamera,
  Scene,
  Light,

  Vector3,
  Mesh,

  HemisphericLight,
  Color3,
  Color4,

  StandardMaterial,
  Texture,
  DynamicTexture,
  Space, BabylonFileLoaderConfiguration
} from '@babylonjs/core';
*/

// IMPORTAJ MI VSE !!!
// BAYBLONJS README -> C:\Users\Bogdan\WebstormProjects\angular-babylonjs\node_modules\@babylonjs\core\readme.md
import * as BABYLON from "@babylonjs/core";
// import 'babylonjs-loaders';
import "@babylonjs/loaders/glTF"; // npm install --save-dev @babylonjs/core @babylonjs/loaders

import "@babylonjs/core/Debug/debugLayer";
import "@babylonjs/inspector";

import Ammo from "ammojs-typed" // ( ena  možnost ki bi morala delat )



// GLEJ: https://www.typescriptlang.org/tsconfig#esModuleInterop , https://stackoverflow.com/questions/71038325/angular-13-package-library-throws-error-moment-is-not-a-function-when-imported
// import Ammo from "ammo.js/builds/ammo.js"; // kot const Ammo = requere(ammo.js);

//// import * as Ammo from "ammo.js/builds/ammo.js";
// import * as Ammo from "ammo.js/builds/ammo.wasm.js";
// import Ammo from "ammo.js";
// import * as Ammo from "ammo.js";
/*
  Problemi:
    BREAKING CHANGE: webpack < 5 used to include polyfills for node.js core modules by default.
    This is no longer the case. Verify if you need this module and configure a polyfill for it.

    If you want to include a polyfill, you need to:
            - add a fallback 'resolve.fallback: { "path": require.resolve("path-browserify") }'
            - install 'path-browserify'

    1.) še prej: naredi custom-webpack moržnost in nastavitev
        https://stackoverflow.com/questions/51068908/angular-cli-custom-webpack-config
        Install the library: npm i -D @angular-builders/custom-webpack

          Modify your angular.json:

          "architect": {
             ...
             "build": {
                 "builder": "@angular-builders/custom-webpack:browser"
                 "options": {
                        "customWebpackConfig": {
                           "path": "./extra-webpack.config.js",
                           "replaceDuplicatePlugins": true
                        },
                        "outputPath": "dist/my-cool-library",
                        "index": "src/index.html",
                        "main": "src/main.ts",
                        "polyfills": "src/polyfills.ts",
                        "tsConfig": "src/tsconfig.app.json"
                        ...
                 }
          Add extra-webpack.config.js to the root of your application
          https://www.justjeb.com/post/customizing-angular-cli-build
          Put the extra configuration inside extra-webpack.config.js (just a plain webpack configuration)
          extra-webpack.config.js (ŠVINGLAJ TUKAJ: https://github.com/RaananW/babylonjs-webpack-es6/blob/master/webpack.common.js)

    2.) Reši:
        As the error states, you should add resolve.fallback: { "path": require.resolve("path-browserify") } in your webpack config file.

        // webpack.config.js

        module.export = {
          ...
          resolve: {
            fallback: {
              path: require.resolve("path-browserify")
            }
          }
          ...
        }


    2. alt: If you don't want to include a polyfill, you can use an empty module like this:
            resolve.fallback: { "path": false }

 */
import {AbstractMesh} from "@babylonjs/core";
// import Ammo from "ammojs-typed";


/*
export enum directionMoveEnum {
  FORWARD,
  BACKWARD,
  LEFT,
  RIGHT
}

export enum KeyEnum {
  KEY_W = 87,
  KEY_S = 83,
  KEY_D = 68,
  KEY_A = 65
}

export enum PointerCameraEnum {
  POINTER_CAMERA_PLUS = 1100,
  POINTER_CAMERA_MINUS = 1101
}/*
*/

@Injectable({ providedIn: 'root' })
export class EngineService {

  // OSNOVA
  private canvas: HTMLCanvasElement;
  private engine: BABYLON.Engine;
  private scene:  BABYLON.Scene;
  private light:  BABYLON.Light;
  private deviceSourceManager: BABYLON.DeviceSourceManager;

  // private camera: BABYLON.FreeCamera;
  public camera: BABYLON.ArcRotateCamera;
  public cameraDistance = 7.5;
  public pushForce = 0.5;
  public angularDrag = 0.95;
  public player: BABYLON.AbstractMesh;
  public playerEmpty: BABYLON.AbstractMesh;
  // private Ammo: typeof AmmoModule; // ena možnost

  // KEYBOARD STUFF

  // lahko bi bil objekt večih stanjk kam naj se gre
  private forward = false;
  private backward = false;
  private left = false;
  private right = false;


  public sceneLoadedFinish = false;
  // private keyMap = new Map<number, String>();


  // public ammo = require('ammo.js/builds/ammo.js');

  // OSTALO
  // private sphere: BABYLON.Mesh;



  public constructor(
    private ngZone: NgZone,
    private windowRef: WindowRefService
    // private ammo: Ammo // to ne gre ne dela
  ) {

  }

  // predelal z (https://playground.babylonjs.com/)
  public async createScene(canvas: ElementRef<HTMLCanvasElement>) { // : void
    this.canvas = canvas.nativeElement;



    // This creates a basic Babylon Scene object (non-mesh)
    this.engine = new BABYLON.Engine(this.canvas, true);
    this.deviceSourceManager = new BABYLON.DeviceSourceManager(this.engine); // ADD/SET/REGISTER DeviceSourceManager na Engine
    this.scene = new BABYLON.Scene(this.engine);

    this.scene.debugLayer.show(); // za babylon 4.2.0
    //this.scene.debugLayer.hide();

    // This creates and positions a free camera (non-mesh)
    //// this.camera = new BABYLON.FreeCamera("camera1", new BABYLON.Vector3(0, 5, -10), this.scene);
    this.camera = new BABYLON.ArcRotateCamera(
      "ArcRotateCamera",
      -Math.PI / 4, (Math.PI / 3),
      this.cameraDistance,
      BABYLON.Vector3.Zero(),
      this.scene );

    // This targets the camera to scene origin
    // this.camera.setTarget(BABYLON.Vector3.Zero());

    // This attaches the camera to the canvas
    this.camera.attachControl(canvas, true);

    // This creates a light, aiming 0,1,0 - to the sky (non-mesh)
    this.light = new BABYLON.HemisphericLight("light", new BABYLON.Vector3(0, 1, 0), this.scene);

    // Default intensity is 1. Let's dim the light a small amount
    this.light.intensity = 0.7;


    const gravityVector = new BABYLON.Vector3(0, -10, 0);
    // glej imnport v angular.json fajlu !!!
    // const physicsPlugin = new BABYLON.CannonJSPlugin();
    // glej imnport v angular.json fajlu !!!
    // const physicsPlugin = new BABYLON.OimoJSPlugin();

    // AMMO PA ŠE NE DELA !!! NE VEM KAKO INCICAILIZIRAT V ANGULAR-JU
    // let Ammo: typeof AmmoModule;


    // ammo.js is a direct port of the Bullet physics engine to JavaScript, using Emscripten
    // https://pybullet.org/wordpress/ (TUKAJ GLEJ API IN DOKUMENTACIJO IN SHOWCASE...)
    // TO DELUJE !! ASTALA-VISTA
    let ammoAPI;
    await import('ammo.js/builds/ammo.js')   // use dynamic import
      .then( (Module) => Module.default() )      // bootstrap ammo.js
      .then((ammo) => {
        const v1 = new ammo.btVector3(1, 2, 3) // use ammo here
        ammoAPI= ammo;
      });
    const physicsPlugin = new BABYLON.AmmoJSPlugin(true, ammoAPI ); // not ready

    this.scene.enablePhysics(gravityVector, physicsPlugin);

    /*
    sphere.physicsImpostor = new BABYLON.PhysicsImpostor(
      sphere,
      BABYLON.PhysicsImpostor.SphereImpostor,
      {
        mass: 1,
        restitution: 0.9
      },
      this.scene);

    ground.physicsImpostor = new BABYLON.PhysicsImpostor(
      ground,
      BABYLON.PhysicsImpostor.BoxImpostor,
      {
        mass: 0,
        restitution: 0.9
      },
      this.scene);
     */

    /*
    ////////
    // 1. ALTERNATIVE - KEYBOARD CONTROLLER
    this.scene.onKeyboardObservable.add((kbInfo) => {
      switch (kbInfo.type) {
        case BABYLON.KeyboardEventTypes.KEYDOWN:
          console.log("KEY DOWN: ", kbInfo.event.key);
          break;
        case BABYLON.KeyboardEventTypes.KEYUP:
          console.log("KEY UP: ", kbInfo.event.code);
          break;
      }
    });
    //////
    */

    /*
    ////// onDeviceConnectedObservable
    // OPAZOVANJE, kdaj in kateri controller-ji se konektajo (KEYBOARD, MOUSE, GAMEPAD ...)
    // to je Observer, ko vključimo ali izključimo: gamepad, keybord, miško na računalnik
    dsm.onDeviceConnectedObservable.add((device) => {

      // which device to use or set up
      switch (device.deviceType) { // 1 - keyboard 2 - mouse
        case BABYLON.DeviceType.Keyboard:
          // currentColor = new BABYLON.Color3(1, 0.5, 0.5);
          // controlsText.color = "red";
          // shieldButton = "Z";
          // fireButton = "Spacebar";
          // boostButton = "X";
          // controlsText.text = `Established link to ${BABYLON.DeviceType[device.deviceType]}\n`;
          // controlsText.text += `Pilot Controls\nShield: ${shieldButton}\nFire: ${fireButton}\nBoost: ${boostButton}`;
          console.log("Device type used:", BABYLON.DeviceType[device.deviceType])
          break;
        case BABYLON.DeviceType.Xbox:
          // currentColor = new BABYLON.Color3(0.5, 1, 0.5);
          // controlsText.color = "green";
          // shieldButton = "A";
          // fireButton = "X";
          // boostButton = "B";
          // controlsText.text = `Established link to ${BABYLON.DeviceType[device.deviceType]}\n`;
          // controlsText.text += `Pilot Controls\nShield: ${shieldButton}\nFire: ${fireButton}\nBoost: ${boostButton}`;

          console.log("Device type used:", BABYLON.DeviceType[device.deviceType])
          break;
      }
    });
    */


    // Append glTF model to scene.
    // http://192.168.1.73:4210/assets/Levels/level_v1.gltf
    // BABYLON.SceneLoader.Append("assets/Levels/", "level_v1.glb", this.scene, function (scene) {

    console.log("-> START LOADING SCENE");
    // await this.loadMyScene(this.scene);
    this.loadMyScene(this.scene);
    console.log("<- END LOADING SCENE (CHECK ASYNC OR SYNC");


    /*
    //---- TEST ZA POINTER (MOUSE WHEEL) EVENT
    this.scene.onPrePointerObservable.add( (pointerInfo, eventState) => {

      // console.log(pointerInfo);
      var event = pointerInfo.event;

      if ( (pointerInfo.event.movementY deltaY > 0) && (this.cameraDistance > 1) ) { // če hočemo zoom-in in je distanca že ena ne naredimo ničesar
        console.log("TAG 1");
        this.cameraDistance += 1
        return;
      }
      //if ((event.wheelDelta / 120 > 0) && (self._cameraDistance > 1)) {
      if ( (event.movementY deltaY < 0) && this.cameraDistance > 1) ) {
        console.log("TAG 2");
        this.cameraDistance -= 1;  // SCROLL UP
        return; // zaključimo !!
      } else {
        console.log("TAG 3");
        //console.log("scroll +1");
        this.cameraDistance += 1;  // SCROLL DOWN
      }
    }, BABYLON.PointerEventTypes.POINTERWHEEL, false);
    //---- TEST ZA POINTER (MOUSE WHEEL) EVENT
    */
    //// this.camera.inputs.addMouseWheel()

    // https://playground.babylonjs.com/#7CBW04
    /////////////////////////// MOUSE SCROOL AND POINTER SCROOL OBSERVER - ZA CAMERA ZOOM IN AND OUT ///////////////////
    // Predvidevamo ali z varvolako da je this.scene != null
    /*this.scene.onPointerObservable.add((pointerInfo) => {
      switch (pointerInfo.type) {
        case BABYLON.PointerEventTypes.POINTERDOWN:
          console.log("1: ", pointerInfo);
          break;
        case BABYLON.PointerEventTypes.POINTERUP:
          console.log("2: ", pointerInfo);
          break;
        case BABYLON.PointerEventTypes.POINTERMOVE:
          console.log("3: ", pointerInfo);
          break;
      }
    });

    this.scene.onPointerObservable.add((pointerInfo) => {
        if (pointerInfo.event.)

      }
    });*/
    /////////////////////////// MOUSE SCROOL AND POINTER SCROOL OBSERVER - ZA CAMERA ZOOM IN AND OUT ///////////////////


    // MAIN/CENTRAL RENDER LOOP
    this.engine.runRenderLoop(() => {

      // po mojem bi moral biti zunaj loop-a

      this.keyboardMouseController(this.deviceSourceManager);

      if (this.sceneLoadedFinish === true) {

        this.poskrbiZaPushOrDragBall();



        this.scene.render();
      }

    });
    // ALI 2.
    // return scene; // ZA BabylonJS Playgrounde <-

  }


  /**
   * CENTRAL CONTROLLER (KEYBORAD, MOUSE. KEYPAD ...)
   * @param deviceSourceManager
   */
  public keyboardMouseController(deviceSourceManager: BABYLON.DeviceSourceManager) {
    // KEYBOARD CONTROLLER -> keys controll-ing
    if (deviceSourceManager.getDeviceSource(BABYLON.DeviceType.Keyboard)) {

      // KATERA TIPKA IMA KATERO KODO - glej: https://keycode.info/
      if (deviceSourceManager.getDeviceSource(BABYLON.DeviceType.Keyboard).getInput(87) === 1) {
        // console.log('W - key Down');
        this.forward = true;
      } else if (deviceSourceManager.getDeviceSource(BABYLON.DeviceType.Keyboard).getInput(87) !== 1) {
        // console.log('W - key Up');
        this.forward = false;
      }
      if (deviceSourceManager.getDeviceSource(BABYLON.DeviceType.Keyboard).getInput(83) === 1) {
        // console.log('S - key Down');
        this.backward = true;
      } else if (deviceSourceManager.getDeviceSource(BABYLON.DeviceType.Keyboard).getInput(83) !== 1) {
        // console.log('S - key Up');
        this.backward = false;
      }
      if (deviceSourceManager.getDeviceSource(BABYLON.DeviceType.Keyboard).getInput(65) === 1) {
        // console.log('A - key Down');
        this.left = true;
      } else if (deviceSourceManager.getDeviceSource(BABYLON.DeviceType.Keyboard).getInput(65) !== 1) {
        // console.log('A - key Up');
        this.left = false;
      }
      if (deviceSourceManager.getDeviceSource(BABYLON.DeviceType.Keyboard).getInput(68) === 1) {
        // console.log('D - key Down');
        this.right = true;
      } else if (deviceSourceManager.getDeviceSource(BABYLON.DeviceType.Keyboard).getInput(68) !== 1) {
        // console.log('D - key Up');
        this.right = false;
      }
    }



    // MOUSE CONTROLLER
    if (deviceSourceManager.getDeviceSource(BABYLON.DeviceType.Mouse)) {
      // console.log("Mouse input: " dsm.getDeviceSource(BABYLON.DeviceType.Mouse).getInput() )
      // BABYLON.ArcRotateCameraMouseWheelInput.
      if (deviceSourceManager.getDeviceSource(BABYLON.DeviceType.Mouse).getInput(BABYLON.PointerInput.LeftClick) === 1) {
        console.log("You click: LMB - Left Mouse Button");
        // console.log(deviceSourceManager.getDeviceSource(BABYLON.DeviceType.Mouse).getInput(BABYLON.PointerInput.Horizontal) + "," + deviceSourceManager.getDeviceSource(BABYLON.DeviceType.Mouse).getInput(BABYLON.PointerInput.Vertical));
      }
      if (deviceSourceManager.getDeviceSource(BABYLON.DeviceType.Mouse).getInput(BABYLON.PointerInput.RightClick) === 1) {
        console.log("You click: RMB - Right Mouse Button");
      }
      if (deviceSourceManager.getDeviceSource(BABYLON.DeviceType.Mouse).getInput(BABYLON.PointerInput.MiddleClick) === 1) {
        console.log("You click: MMB - Middle Mouse Button");
      }
    }

    /*
    // VAROVALKA (da je this.player ziher definiran !!!
    if (this.player) { // <- VAROVALKA (da je this.player ziher definiran
      // this.camera.setTarget(this.player.position);

      // this.camera.setTarget(this.player.position);
      // this.camera.radius = this.cameraDistance;
      // this.camera.setPosition(this.player.position);
      // this.camera.radius = this.cameraDistance;

      // this.camera.radius = this.cameraDistance;
      // this.camera.position = this.player.position -10;

      this.camera.radius = this.cameraDistance;
      this.camera.setTarget(this.player.position);
    }
    // this.camera.radius = this.cameraDistance;
    // poskrbiZaPushOnDragBall()...
    // self._camera.radius = self._cameraDistance;

    // this.camera.radius = this.cameraDistance;
    */

  }
  //// END METHOD !!!




  /**
   *
   * METHOD - utility for colliders for gltf laoder
   * @param scene
   * @param entity
   * @param impostor
   * @param options
   * @param reparent
   */

  public createPhysicsImpostor(scene, entity, impostor, options, reparent) {
    if (entity == null)
      return;
    entity.checkCollisions = false;
    const parent = entity.parent;
    if (reparent === true)
      entity.parent = null;
    entity.physicsImpostor = new BABYLON.PhysicsImpostor(entity, impostor, options, scene);
    // NIMAM POJMA KAJ TA DEL KOMENTIRANE KODE BIL SPLOH MIKŠLJEN s strani #MackeyK24:
    // https://forum.babylonjs.com/u/mackeyk24/summary
    // na forumu tukaj: https://forum.babylonjs.com/t/loading-gltf-and-physics-not-working-what-am-i-missing/4878
    // if (reparent === true)
      // entity.parent = parent;
  };

  /**
  * @method
  * @param inScene
  * METHOD - Loading scene & Set colliders (SphereCollider, BoxCollider, MeshCollider)
  */
  public loadMyScene(inScene) {

      // , await ( (scene) => {
      // BABYLON.SceneLoader.Append("assets/Levels/", "level_v1.gltf", inScene, (scene) => {
      // VEDNO LOAD-AJ .glb file !!!
      BABYLON.SceneLoader.Append("assets/Levels/", "level_v1.glb", inScene, (scene) => {

        console.log("You Load .gltf file scene: ", scene.meshes);

        const meshes = scene.meshes; // : BABYLON.AbstractMesh[]
        // for (const it in meshes) {
        for (let i = 0; i < meshes.length; i++) {
          const meshIt = meshes[i];
          // dodaj Colliders (SphereCollider, BoxCollider)
          if (meshIt.name === "Player_with_sphere_collider") {
            console.log("SphareCollider - Dynamic (Player) : ", meshIt.name);
            this.player = meshIt;
            this.camera.setTarget(this.player.position)
            this.createPhysicsImpostor(scene, meshIt, BABYLON.PhysicsImpostor.SphereImpostor, {
              mass: 1.0,
              friction: 1.0,
              restitution: 1.5
            }, true);
            this.playerEmpty = BABYLON.Mesh.CreateBox("box", 1.6, scene);
            this.playerEmpty.visibility = 0;
            this.playerEmpty.position = this.player.position; // playerEmpty-ju je starš this.player (this.player = je starš, this.playerEmpty = child od this.player)

            // this.playerEmpty.rotation = this.player.rotation;
            //this.playerEmpty.rotationQuaternion = this.playerEmpty.rotationQuaternion;

            // this.playerEmpty.parent = this.player;

            // this.player.rotation = this.playerEmpty.rotation;
            // this.playerEmpty.rotation = this.player.rotation;
            // this.playerEmpty.rotationQuaternion = this.playerEmpty.rotationQuaternion);

          } else if (meshIt.name.indexOf("_with_sphere_collider") !== -1) {
            console.log("SphareCollider - Static: ", meshIt.name);
            // this.createPhysicsImpostor(scene, mesh, BABYLON.PhysicsImpostor.SphereImpostor, { mass: 0, friction: 0.5, restitution: 0 }, true);
          } else if (meshIt.name.indexOf("_with_box_collider") !== -1) {
            console.log("BoxCollier - Static: ", meshIt.name);
            this.createPhysicsImpostor(scene, meshIt, BABYLON.PhysicsImpostor.BoxImpostor, {
              mass: 0,
              friction: 1.0,
              restitution: 0
            }, true);
          } else if (meshIt.name.indexOf("_with_mesh_collider") !== -1) {
            console.log("MeshCollier - Static: ", meshIt.name);
            this.createPhysicsImpostor(scene, meshIt,
              BABYLON.PhysicsImpostor.MeshImpostor,
              {
                mass: 0,
                friction: 1.0,
                restitution: 0
              },true);
          } else if (meshIt.name.indexOf("_with_convex_hull_collider") !== -1) {
            console.log("MeshCollier - Static: ", meshIt.name);
            this.createPhysicsImpostor(scene, meshIt,
              BABYLON.PhysicsImpostor.ConvexHullImpostor,
              {
                mass: 0,
                friction: 1.0,
                restitution: 0
              },true);
          }
          /*
          this.createPhysicsImpostor(this.scene, mesh, BABYLON.PhysicsImpostor.BoxImpostor, {
            mass: 1,
            restitution: 0.9
          }, true);
          */

        } // END FOR

        this.sceneLoadedFinish = true;

      }); // END loadMyScene


  }
  // ==== END METHOD public async loadMyScene(inScene)

  /**
   *
   */
  poskrbiZaPushOrDragBall() {
    // console.log("poskrbiZaRotacijo()...");

    let angularVelocity = this.player.physicsImpostor.getAngularVelocity(); // angularVelocity je Vector3
    let cameraMatrix = this.camera.getWorldMatrix();

    // IF ANY OF THE MOVEMENT KEYS W,S,D,A ARE DOWN ADD VELOCITY TO FINALAXIS
    if ( this.forward ) {
      let finalAxis = (BABYLON.Vector3.TransformNormal(new BABYLON.Vector3(1, 0, 0), cameraMatrix));
      finalAxis.x *= this.pushForce;
      finalAxis.y *= this.pushForce;
      finalAxis.z *= this.pushForce;

      angularVelocity.x += finalAxis.x;
      angularVelocity.y += finalAxis.y;
      angularVelocity.z += finalAxis.z;
    }
    if (this.backward) {
      let finalAxis = (BABYLON.Vector3.TransformNormal(new BABYLON.Vector3(-1, 0, 0), cameraMatrix));
      finalAxis.x *= this.pushForce;
      finalAxis.y *= this.pushForce;
      finalAxis.z *= this.pushForce;

      angularVelocity.x += finalAxis.x;
      angularVelocity.y += finalAxis.y;
      angularVelocity.z += finalAxis.z;
    }
    if (this.right) {
      let finalAxis = (BABYLON.Vector3.TransformNormal(new BABYLON.Vector3(0, 0, -1), cameraMatrix));
      finalAxis.x *= this.pushForce;
      finalAxis.y *= this.pushForce;
      finalAxis.z *= this.pushForce;

      angularVelocity.x += finalAxis.x;
      angularVelocity.y += finalAxis.y;
      angularVelocity.z += finalAxis.z;
    }
    if (this.left) {
      let finalAxis = (BABYLON.Vector3.TransformNormal(new BABYLON.Vector3(0, 0, 1), cameraMatrix));
      finalAxis.x *= this.pushForce;
      finalAxis.y *= this.pushForce;
      finalAxis.z *= this.pushForce;

      angularVelocity.x += finalAxis.x;
      angularVelocity.y += finalAxis.y;
      angularVelocity.z += finalAxis.z;
    }

    // APPLY ANGULAR DRAG
    angularVelocity.x = angularVelocity.x * this.angularDrag;
    angularVelocity.y = angularVelocity.y * this.angularDrag;
    angularVelocity.z = angularVelocity.z * this.angularDrag;
    //angularVelocity.w = angularVelocity.w * self._angular_drag;

    // SET ANGULAR VELOCITY
    this.player.physicsImpostor.setAngularVelocity(angularVelocity);
    this.playerEmpty.position = new BABYLON.Vector3(this.player.position.x, this.player.position.y, this.player.position.z) ;

    let dir = this.playerEmpty.position.subtract(this.camera.position);
    dir = dir.normalize();

    // this.camera.position = this.playerEmpty.position;
    this.camera.setTarget(this.playerEmpty);
    this.camera.radius = this.cameraDistance;



  }
  // ==== END METHOD public async loadMyScene(inScene)


}

