import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';

import {UiInfobarTopComponent} from "./ui/ui-infobar-top/ui-infobar-top.component";
import {EngineComponent} from "./engine/engine.component";
/*
import {ActFormComponent} from './components/act-form/act-form.component';
import {ActTableComponent} from './components/act-table/act-table.component';
import {TestBootstrapAngComponent} from './components/test-bootstrap-ang/test-bootstrap-ang.component';
*/


// import { InvpInventuraPeFormComponent } from './components/invp-inventura-pe-form/invp-inventura-pe-form.component';
// import { InvpInventuraPeTableComponent } from './components/invp-inventura-pe-table/invp-inventura-pe-table.component';

// #IMPORT_COMPONENTS

const routes: Routes = [
  // DEFAULT ROUTE
  {
    path: '',
    component: EngineComponent,
  },

  {
    path: 'app-engine',
    component: EngineComponent,
  },

  {
    path: 'test1',
    component: UiInfobarTopComponent,
  },

    /*
    {
        path: 'invp-inventura-pe-form',
        component: InvpInventuraPeFormComponent,
    },
    {
        path: 'invp-inventura-pe-table',
        component: InvpInventuraPeTableComponent,
    },
    */


    // #ADD_ROUTING

];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})

export class AppRoutingModule {
}
