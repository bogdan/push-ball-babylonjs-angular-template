import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from "./app-routing.module";

import { WindowRefService } from './services/window-ref.service';

import { AppComponent } from './app.component';
import { EngineComponent } from './engine/engine.component';

import { UiInfobarBottomComponent } from './ui/ui-infobar-bottom/ui-infobar-bottom.component';
import { UiInfobarTopComponent } from './ui/ui-infobar-top/ui-infobar-top.component';
import { UiSidebarLeftComponent } from './ui/ui-sidebar-left/ui-sidebar-left.component';
import { UiSidebarRightComponent } from './ui/ui-sidebar-right/ui-sidebar-right.component';
import { UiComponent } from './ui/ui.component';

import {RouterModule} from "@angular/router";

// AmmoModule (import OK pravilno) !!
// import AmmoModule from "ammojs-typed"; // ga importamo (to je pravilno inpportano)
// import Ammo from "ammojs-typed"; // je tuti ok



@NgModule({
  declarations: [
    AppComponent,
    EngineComponent,

    UiSidebarLeftComponent,
    UiSidebarRightComponent,
    UiInfobarTopComponent,
    UiComponent,
    UiInfobarBottomComponent,



  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule
  ],
  providers: [
    WindowRefService
  ],
  bootstrap: [
    AppComponent
  ]
})
export class AppModule {
  // Ammo // ga exportamo (to je pravilno exportano (zdaj lahko uporabljamo v komponentah ali ko injection v construktor-jih
  //
}
